#include "../includes/header.h"

#define BUFF_FILE 10000

static t_game	save_player(t_game game, int w, int h)
{
	if (game.p.exist == false)
	{
		game.p.pos_x = w;
		game.p.pos_y = h;
		game.p.last_x = w;
		game.p.last_y = h;
		game.p.exist = true;
	}
	else
	{
		free_game(&game);
		quit_game(ERR_M1, EXIT_FAILURE);
	}
	return (game);
}

static t_game	save_element(t_game game)
{
	int	h;
	int	w;

	h = 0;
	while (game.map.tab[h])
	{
		w = 0;
		while (game.map.tab[h][w])
		{
			if (ft_strrchr("P", game.map.tab[h][w]))
				game = save_player(game, w, h);
			if (ft_strrchr("E", game.map.tab[h][w]))
				game.e.exist = true;
			if (ft_strrchr("C", game.map.tab[h][w]))
			{
				game.c.to_catch++;
				game.c.exist = true;
			}
			w++;
		}
		h++;
	}
	return (game);
}

static t_game	save_n_verif(t_game game)
{
	verif_char(game);
	verif_close(game);
	game = save_element(game);
	verif_valid(game);
	return (game);
}

static t_game	save_size(t_game game)
{
	int	h;
	int	w;

	h = 0;
	w = -1;
	while (game.map.tab[h])
	{
		game.map.width = ft_strlen(game.map.tab[h]);
		game.map.height = h + 1;
		if (w <= 0)
			w = game.map.width;
		if (game.map.width != w)
		{
			free_game(&game);
			quit_game(ERR_M0, EXIT_FAILURE);
		}
		h++;
	}
	if (h == 0)
	{
		free_game(&game);
		quit_game(ERR_M6, EXIT_SUCCESS);
	}
	return (game);
}

t_game	save_map(t_game game, char *argv[], int fd)
{
	char	buffer[BUFF_FILE];
	char	*str;
	ssize_t	ret;

	fd = open(argv[1], O_RDONLY);
	if (fd == -1)
	{
		close(fd);
		quit_game(ERR_A1, EXIT_FAILURE);
	}
	ret = read(fd, &buffer, BUFF_FILE);
	if (ret == -1)
	{
		close(fd);
		quit_game(ERR_A5, EXIT_FAILURE);
	}
	buffer[ret] = '\0';
	close(fd);
	str = ft_strdup(buffer);
	verif_return(str, 0);
	game.map.tab = ft_split(str, '\n');
	free(str);
	game = save_size(game);
	game = save_n_verif(game);
	return (game);
}
