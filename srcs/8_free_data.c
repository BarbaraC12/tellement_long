#include "../includes/header.h"

void	quit_game(char *txt, int err_code)
{
	ft_putstr(txt);
	exit(err_code);
}

void	free_game(t_game *game)
{
	int	i;

	i = 0;
	if (game->map.tab)
	{
		while (game->map.tab[i])
			free(game->map.tab[i++]);
		free(game->map.tab);
	}
	if (game->data.mlx)
		free(game->data.mlx);
}

void	quit_init_mlx(t_game *g, char *text, int err)
{
	if (g->data.txt.player.ptr)
		mlx_destroy_image(g->data.mlx, g->data.txt.player.ptr);
	if (g->data.txt.exit.ptr)
		mlx_destroy_image(g->data.mlx, g->data.txt.exit.ptr);
	if (g->data.txt.floor.ptr)
		mlx_destroy_image(g->data.mlx, g->data.txt.floor.ptr);
	if (g->data.txt.obj.ptr)
		mlx_destroy_image(g->data.mlx, g->data.txt.obj.ptr);
	if (g->data.txt.wall.ptr)
		mlx_destroy_image(g->data.mlx, g->data.txt.wall.ptr);
	if (g->data.txt.move.ptr)
		mlx_destroy_image(g->data.mlx, g->data.txt.move.ptr);
	if (g->data.mlx_win)
		mlx_destroy_window(g->data.mlx, g->data.mlx_win);
	if (g->data.mlx)
		mlx_destroy_display(g->data.mlx);
	mlx_loop_end(g->data.mlx);
	free_game(g);
	quit_game(text, err);
}

int	quit_process(t_game *g)
{
	mlx_destroy_image(g->data.mlx, g->data.txt.exit.ptr);
	mlx_destroy_image(g->data.mlx, g->data.txt.wall.ptr);
	mlx_destroy_image(g->data.mlx, g->data.txt.player.ptr);
	mlx_destroy_image(g->data.mlx, g->data.txt.obj.ptr);
	mlx_destroy_image(g->data.mlx, g->data.txt.floor.ptr);
	mlx_destroy_image(g->data.mlx, g->data.txt.move.ptr);
	mlx_destroy_window(g->data.mlx, g->data.mlx_win);
	mlx_destroy_display(g->data.mlx);
	mlx_loop_end(g->data.mlx);
	free_game(g);
	exit(EXIT_SUCCESS);
	return (0);
}
