#include "../includes/header.h"

static t_game	*verif_pos(t_game *g, int i, int j)
{
	if (g->map.tab[i][j] == 'C')
	{
		g->c.catch++;
		g->map.tab[i][j] = '0';
	}
	if (g->map.tab[i][j] == 'E')
	{
		if (g->c.catch == g->c.to_catch)
			quit_process(g);
	}
	return (g);
}

static void	player_draw(t_game *g)
{
	int	i;
	int	j;

	i = 0;
	while (g->map.tab[i])
	{
		j = 0;
		while (g->map.tab[i][j])
		{
			if (g->map.tab[i][j] == '0' || g->map.tab[i][j] == 'P' )
				mlx_put_image_to_window(g->data.mlx, g->data.mlx_win,
					g->data.txt.floor.ptr, g->caze * j, g->caze * i);
			else if (g->map.tab[i][j] == 'E')
				mlx_put_image_to_window(g->data.mlx, g->data.mlx_win,
					g->data.txt.exit.ptr, g->caze * j, g->caze * i);
			if (i == g->p.pos_y && j == g->p.pos_x)
				mlx_put_image_to_window(g->data.mlx, g->data.mlx_win,
					g->data.txt.player.ptr, g->caze * j, g->caze * i);
			j++;
		}
		i++;
	}
}

int	map_draw(t_game *g)
{
	int	i;
	int	j;

	i = 0;
	while (g->map.tab[i] != NULL)
	{
		j = 0;
		while (g->map.tab[i][j])
		{
			if (g->map.tab[i][j] == '1')
				mlx_put_image_to_window(g->data.mlx, g->data.mlx_win,
					g->data.txt.wall.ptr, g->caze * j, g->caze * i);
			else if (g->map.tab[i][j] == 'E')
				mlx_put_image_to_window(g->data.mlx, g->data.mlx_win,
					g->data.txt.exit.ptr, g->caze * j, g->caze * i);
			else if (g->map.tab[i][j] == 'C')
				mlx_put_image_to_window(g->data.mlx, g->data.mlx_win,
					g->data.txt.obj.ptr, g->caze * j, g->caze * i);
			j++;
		}
		i++;
	}
	return (0);
}

int	prime_draw(t_game *g)
{
	map_draw(g);
	player_draw(g);
	mlx_put_image_to_window(g->data.mlx, g->data.mlx_win,
		g->data.txt.move.ptr, 0, 0);
	mlx_set_font(g->data.mlx, g->data.mlx_win, FONT_G);
	mlx_string_put(g->data.mlx, g->data.mlx_win, 25, 18, 0x00000000, "0");
	return (0);
}

int	game_draw(t_game *g)
{
	char	*move;

	g = verif_pos(g, g->p.pos_y, g->p.pos_x);
	player_draw(g);
	move = ft_itoa(g->move);
	mlx_put_image_to_window(g->data.mlx, g->data.mlx_win,
		g->data.txt.move.ptr, 0, 0);
	mlx_set_font(g->data.mlx, g->data.mlx_win, FONT_G);
	mlx_string_put(g->data.mlx, g->data.mlx_win, 25, 18, 0x00000000, move);
	free(move);
	return (0);
}
