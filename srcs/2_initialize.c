#include "../includes/header.h"

static t_game	set_size(t_game g)
{
	int	h;
	int	w;

	g.caze = 28;
	h = g.caze * g.map.height;
	w = g.caze * g.map.width;
	mlx_get_screen_size(g.data.mlx, &g.data.screen_w, &g.data.screen_h);
	if (w > g.data.screen_w || h > g.data.screen_h)
		printf("Warning! resolution larger than screen ... Resizing\n");
	g.data.screen_w = *(int *)ft_ternary((w > g.data.screen_w),
			&g.data.screen_w, &w);
	g.data.screen_h = *(int *)ft_ternary((h > g.data.screen_h),
			&g.data.screen_h, &h);
	return (g);
}

static t_img	load_image(char *pathfile, t_game g)
{
	t_img	img;

	img.ptr = mlx_xpm_file_to_image(g.data.mlx, pathfile, &img.w, &img.h);
	if (!img.ptr)
		quit_init_mlx(&g, ERR_T2, EXIT_FAILURE);
	if (img.h != 28)
	{
		mlx_destroy_image(g.data.mlx, img.ptr);
		quit_init_mlx(&g, ERR_T1, EXIT_SUCCESS);
	}
	img.addr = mlx_get_data_addr(img.ptr, &img.bits_per_pixel,
			&img.line_length, &img.endian);
	if (img.addr == NULL)
		quit_init_mlx(&g, ERR_T0, EXIT_FAILURE);
	return (img);
}

t_game	initialize_game(void)
{
	t_game	game;

	game.map.width = -1;
	game.map.height = -1;
	game.valid = false;
	game.p.exist = false;
	game.e.exist = false;
	game.c.exist = false;
	game.c.catch = 0;
	game.c.to_catch = 0;
	game.map.tab = '\0';
	game.move = 0;
	ft_bzero(&game.data.mlx, sizeof(void *));
	ft_bzero(&game.data.txt, sizeof(t_imgs));
	return (game);
}

t_game	init_mlx(t_game g)
{
	g.data.mlx = mlx_init();
	if (!g.data.mlx)
	{
		free_game(&g);
		quit_game(ERR_A3, EXIT_FAILURE);
	}
	g = set_size(g);
	g.data.mlx_win = mlx_new_window(g.data.mlx, g.data.screen_w,
			g.data.screen_h, EXE);
	g.data.txt.player = load_image(PLAYER, g);
	g.data.txt.exit = load_image(EXIT, g);
	g.data.txt.floor = load_image(FLOOR, g);
	g.data.txt.obj = load_image(OBJET, g);
	g.data.txt.wall = load_image(WALL, g);
	g.data.txt.move = load_image(BANDEROL, g);
	g.valid = true;
	return (g);
}
