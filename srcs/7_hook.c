#include <X11/X.h>
#include "../includes/header.h"

static int	minimize_request(t_game *g)
{
	map_draw(g);
	game_draw(g);
	return (1);
}

static int	verif_wall(char **tab, int i, int j)
{
	if (tab[i][j] == '1')
		return (0);
	return (1);
}

static int	key_pressed(int keycode, t_game *g)
{
	g->p.last_x = g->p.pos_x;
	g->p.last_y = g->p.pos_y;
	if (keycode == ESC)
		quit_process(g);
	if (keycode == A || keycode == D || keycode == W || keycode == S)
	{
		if (keycode == A && verif_wall(g->map.tab, g->p.pos_y, g->p.pos_x - 1))
			g->p.pos_x--;
		else if (keycode == D && verif_wall(g->map.tab, g->p.pos_y,
				      g->p.pos_x + 1))
			g->p.pos_x++;
		else if (keycode == W && verif_wall(g->map.tab, g->p.pos_y - 1,
				g->p.pos_x))
			g->p.pos_y--;
		else if (keycode == S && verif_wall(g->map.tab, g->p.pos_y + 1,
				g->p.pos_x))
			g->p.pos_y++;
		g->move++;
		printf("move: %d\n", g->move);
		game_draw(g);
	}
	return (0);
}

t_game	*hook_event(t_game *g)
{
	mlx_hook(g->data.mlx_win, 02, 1L << 0, key_pressed, &g->data);
	mlx_hook(g->data.mlx_win, 33, 1L << 17, quit_process, &g->data);
	mlx_hook(g->data.mlx_win, 12, 1L << 15, minimize_request, &g->data);
	mlx_loop_hook(g->data.mlx, NULL, &g->data);
	mlx_loop(g->data.mlx);
	return (g);
}
