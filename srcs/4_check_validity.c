#include "../includes/header.h"

void	verif_close(t_game game)
{
	int	h;
	int	w;

	h = 0;
	while (game.map.tab[h])
	{
		w = 0;
		while (game.map.tab[h][w])
		{
			if (!(ft_strrchr("1", game.map.tab[0][w]))
					|| !(ft_strrchr("1", game.map.tab[h][0]))
					|| !(ft_strrchr("1", game.map.tab[h][game.map.width]))
					|| !(ft_strrchr("1", game.map.tab[game.map.height - 1][w])))
			{
				free_game(&game);
				quit_game(ERR_M4, EXIT_FAILURE);
			}
			else
				w++;
		}
		h++;
	}
}

void	verif_char(t_game game)
{
	int	h;
	int	w;

	h = 0;
	while (game.map.tab[h])
	{
		w = 0;
		while (game.map.tab[h][w])
		{
			if (!(ft_strrchr("10EPC", game.map.tab[h][w])))
			{
				free_game(&game);
				quit_game(ERR_M5, EXIT_FAILURE);
			}
			w++;
		}
		h++;
	}
}

void	verif_valid(t_game game)
{
	if (game.p.exist == false)
	{
		free_game(&game);
		quit_game(ERR_M1, EXIT_FAILURE);
	}
	else if (game.e.exist == false)
	{
		free_game(&game);
		quit_game(ERR_M2, EXIT_FAILURE);
	}
	else if (game.c.to_catch == 0)
	{
		free_game(&game);
		quit_game(ERR_M3, EXIT_FAILURE);
	}
}

static void	quit_verif(char *str)
{
	free(str);
	quit_game(ERR_M7, EXIT_FAILURE);
}

void	verif_return(char *str, int i)
{
	while (str[i] == '\n')
		i++;
	while (str[i])
	{
		while (str[i++])
		{
			while (str[i] == '1' || str[i] == '0' || str[i] == 'E'
				|| str[i] == 'P' || str[i] == 'C')
				i++;
			if (str[i] == '\0')
				return ;
			if (str[i++] == '\n')
			{
				if (ft_strrchr("10EPC", str[i]))
					break ;
				while (str[i] == '\n')
					i++;
				if (str[i] == '\0')
					return ;
				if (ft_strrchr("10EPC", str[i]))
					quit_verif(str);
			}
		}
	}
}
