#include "../includes/libft.h"

void	*ft_ternary(int cond, void *rue, void *flse)
{
	if (cond)
		return (rue);
	return (flse);
}

void	ft_putstr(char *s)

{
	if (!s)
		return ;
	while (*s)
	{
		write(1, s, 1);
		s++;
	}
}

size_t	ft_strlen(const char *s)
{
	size_t	i;

	i = 0;
	while (s[i] != '\0')
		i++;
	return (i);
}

int	ft_countw(const char *str, char c)
{
	int	word;

	word = 0;
	if (*str != c && *str)
	{
		str++;
		word++;
	}
	while (*str)
	{
		while (*str == c)
		{
			str++;
			if (*str == '\0')
				return (word);
			if (*str != c && *str)
				word++;
		}
		str++;
	}
	return (word);
}

int	ft_lenw(const char *str, char c)
{
	int	count;

	count = 0;
	while (*str != c && *str)
	{
		count++;
		str++;
	}
	return (count);
}
