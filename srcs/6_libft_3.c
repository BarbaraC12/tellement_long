#include "../includes/libft.h"

static void	*free_it(char **spt)
{
	int		i;

	i = 0;
	while (spt[i])
	{
		free(spt[i++]);
	}
	free(spt);
	return (NULL);
}

char	**ft_split(char const *s, char c)
{
	int		j;
	int		i;
	char	**spt;

	j = 0;
	spt = (char **)malloc(sizeof(char *) * (ft_countw(s, c) + 1));
	if (!s || !spt)
		return (NULL);
	while (*s)
	{
		while (*s == c && *s)
			s++;
		if (*s != c && *s)
		{
			i = 0;
			spt[j] = (char *)malloc(sizeof(char) * (ft_lenw(s, c) + 1));
			if (!spt[j])
				return (free_it(spt));
			while (*s && *s != c)
				spt[j][i++] = (char)*s++;
			spt[j++][i] = '\0';
		}
	}
	spt[j] = NULL;
	return (spt);
}

static int	int_len(int nbr)
{
	int				count;
	unsigned int	nb;

	if (nbr == 0)
		return (1);
	count = 0;
	nb = nbr;
	if (nbr < 0)
	{
		count++;
		nb *= -1;
	}
	while (nb > 0)
	{
		nb /= 10;
		count++;
	}
	return (count);
}

static char	*number(char *str, int len, long int n)
{
	long int	unbr;
	int			is_neg;

	str[len] = '\0';
	unbr = n;
	is_neg = 0;
	if (n < 0)
	{
		is_neg = 1;
		str[0] = '-';
		unbr = -n;
	}
	len--;
	while (len >= is_neg)
	{
		str[len] = unbr % 10 + '0';
		unbr /= 10;
		len--;
	}
	return (str);
}

char	*ft_itoa(int n)
{
	char			*str;
	int				len;

	len = int_len(n);
	str = (char *)malloc(sizeof(char) * (len + 1));
	if (!str)
		return (NULL);
	str = number(str, len, n);
	return (str);
}
