#include "../includes/header.h"

int	main(int argc, char *argv[])
{
	t_game	g;

	if (argc == 2)
	{
		if (check_arg(argv[1]) != 0)
			quit_game(ERR_A1, EXIT_FAILURE);
		g = initialize_game();
		g = save_map(g, argv, 0);
		g = init_mlx(g);
		if (g.valid == true)
		{
			prime_draw(&g);
			hook_event(&g);
			mlx_loop(g.data.mlx);
		}
		quit_process(&g);
		return (0);
	}
	quit_game(ERR_A0, EXIT_FAILURE);
	return (1);
}
