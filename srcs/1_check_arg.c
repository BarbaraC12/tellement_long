#include "../includes/header.h"

static void	free_dup(char *var, char **tab)
{
	int	i;

	i = 0;
	if (tab)
	{
		while (tab[i])
			free(tab[i++]);
		free(tab);
	}
	if (var)
		free(var);
}

static void	dup_exit(char *txt, char *var, char **tab, int err_code)
{
	free_dup(var, tab);
	ft_putstr(txt);
	exit(err_code);
}

int	check_arg(char *arg)
{
	char	*tmp;
	char	**tab;
	int		last;

	tmp = ft_strdup(arg);
	last = ft_countw(tmp, 47) - 1;
	tab = ft_split(tmp, 47);
	if (ft_strncmp(ft_strrev(tab[last]), "reb.", 4) != 0)
		dup_exit(ERR_A2, tmp, tab, EXIT_FAILURE);
	if (ft_strlen(tab[last]) < 5)
		dup_exit(ERR_A6, tmp, tab, EXIT_FAILURE);
	free_dup(tmp, tab);
	return (0);
}
