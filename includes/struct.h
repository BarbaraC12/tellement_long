#ifndef STRUCT_H
# define STRUCT_H

# include "header.h"

typedef int		t_bool;

typedef struct s_object
{
	int		catch;
	int		to_catch;
	t_bool	exist;
}t_object;

typedef struct s_player
{
	int		pos_x;
	int		last_x;
	int		pos_y;
	int		last_y;
	t_bool	exist;
}t_player;

typedef struct s_exit
{
	t_bool	exist;
	t_bool	touched;
}t_exit;

typedef struct s_map
{
	char	**tab;
	int		width;
	int		height;
}t_map;

typedef struct s_img
{
	void	*ptr;
	char	*addr;
	int		bits_per_pixel;
	int		line_length;
	int		endian;
	int		w;
	int		h;
}t_img;

typedef struct s_imgs
{
	t_img	player;
	t_img	exit;
	t_img	obj;
	t_img	wall;
	t_img	floor;
	t_img	move;
}t_imgs;

typedef struct s_data
{
	void	*mlx;
	void	*mlx_win;
	t_imgs	txt;
	int		screen_w;
	int		screen_h;
}t_data;

#endif
