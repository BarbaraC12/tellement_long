#ifndef HEADER_H
# define HEADER_H
# define _XOPEN_SOURCE
# define __USE_XOPEN

# include <limits.h>
# include <math.h>
# include <stdarg.h>
# include <stdbool.h>
# include <stdio.h>
# include <stdlib.h>
# include <fcntl.h>
# include <sys/stat.h>
# include <sys/types.h>
# include "error.h"
# include "keycode.h"
# include "struct.h"
# include "libft.h"

# define EXE "so_long"
# define PLAYER "./xpm/kingpig.xpm"
# define EXIT "./xpm/canon.xpm"
# define FLOOR "./xpm/floor.xpm"
# define OBJET "./xpm/objet.xpm"
# define WALL "./xpm/wall.xpm"
# define BANDEROL "./xpm/banderole.xpm"
# define FONT_G "-misc-fixed-medium-r-normal--13-120-75-75-c-70-iso8859-1"

typedef struct s_game
{
	t_data		data;
	t_map		map;
	t_player	p;
	t_exit		e;
	t_object	c;
	t_bool		valid;
	int			caze;
	int			move;
}t_game;

int		check_arg(char *arg);

t_game	initialize_game(void);

t_game	save_map(t_game game, char *argv[], int fd);

void	verif_valid(t_game game);
void	verif_char(t_game game);
void	verif_close(t_game game);
void	verif_return(char *str, int i);

t_game	init_mlx(t_game game);

int		map_draw(t_game *g);
int		game_draw(t_game *g);
int		prime_draw(t_game *g);

t_game	*hook_event(t_game *g);

void	quit_game(char *txt, int err_code);
void	free_game(t_game *game);
int		quit_process(t_game *game);
void	quit_init_mlx(t_game *g, char *text, int code_err);

#endif
