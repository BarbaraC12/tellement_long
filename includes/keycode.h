#ifndef KEYCODE_H
# define KEYCODE_H

# ifdef __linux
#  include "../mlx_linux/mlx.h"
#  define ESC		65307
#  define W			119
#  define S			115
#  define A			97
#  define D			100
#  define UP		65362
#  define DOWN		65364
#  define RIGHT     65363
#  define LEFT		65361
#  ifndef BONUS
#   define BONUS	0
#  else 
#   define BONUS	1
#   include "bonus.h"
#  endif
# elif __APPLE__
#  include "../mlx_OpenGL/mlx.h"
#  define ESC	53
#  define RIGHT	124
#  define LEFT	123
#  define W		13
#  define S		1
#  define A		0
#  define D		2
# endif

#endif
