#ifndef ERROR_H
# define ERROR_H

# define ERR_A0 "./so_long: Usage ./so_long [path_map.ber]\n"
# define ERR_A1 "./so_long: Open faillure\n"
# define ERR_A2 "./so_long: Extension needs to be .ber\n"
# define ERR_A3 "./so_long: Problem at initialization\n"
# define ERR_A4 "./so_long: Something went wrong when close\n"
# define ERR_A5 "./so_long: File is not readable\n"
# define ERR_A6 "./so_long: .ber not extention but hidden file\n"

# define ERR_M0 "./so_long: Map not rectangular\n"
# define ERR_M1 "./so_long: Map must have 1 player no more no less\n"
# define ERR_M2 "./so_long: Map must have at least 1 exit\n"
# define ERR_M3 "./so_long: Map must have at least 1 collectible\n"
# define ERR_MM "./so_long: Project made by bcano from 42Paris\n"
# define ERR_M4 "./so_long: Map must be surrounded by walls\n"
# define ERR_M5 "./so_long: Invalid char: is accepted >> P, C, E, 1, 0\n"
# define ERR_M6 "./so_long: Map is empty\n"
# define ERR_M7 "./so_long: Multi return in map file\n"

# define ERR_T0 "./so_long: Texture not initialized\n"
# define ERR_T1 "./so_long: Bad size texture: need to be at 28px\n"
# define ERR_T2 "./so_long: Bad path for texture\n"

#endif
