#ifndef LIBFT_H
# define LIBFT_H

# include <stdio.h>
# include <string.h>
# include <stdlib.h>
# include <unistd.h>

char	*ft_itoa(int n);
char	*ft_strrchr(const char *s, int c);
void	ft_bzero(void *s, size_t n);
int		ft_strncmp(const char *s1, const char *s2, size_t n);
char	**ft_split(char const *s, char c);
int		ft_lenw(const char *str, char c);
int		ft_countw(const char *str, char c);
size_t	ft_strlen(const char *s);
char	*ft_strdup(const char *s1);
void	ft_putstr(char *s);
char	*ft_strrev(char *str);
void	*ft_ternary(int cond, void *rue, void *flse);

#endif
