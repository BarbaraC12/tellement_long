# Pig mini Game

This is a 42 school's graphical design project.

:warning: **Warning**: It is educational project.

:warning: **Warning**: You can take inspiration from it but please don't copy / paste what you don't understand.

## SUMMARY :

This project is a very small 2D game. It is built to make you work with textures, sprites. And some very basic gameplay elements.

This project will enable you to improve your skills like management of windows, colors, events, textures, parsing, ... .

#### Render:
![img](https://raw.githubusercontent.com/BarbaraC12/mini_jeu/main/finalRender.png?token=ANJMLLLLSDCAXV6QXXCHE2DBOMKYO)

## Don't forget to check :

On LINUX use valgrind to check if your program LEAKS

#### Arguments

- Number of arguments different from 2 (`Executable` + `root_map.ber`
- Test if the map is a file with a '.ber' extension
- Test the behavior if the map test is a folder with a '.ber' extension
- Test if map file does not exist

#### Parsing

- Test with an unclose map
- Test with unauthorized characters (Only 0, 1, C, E, P are autorized)
- Test with whitespaces
- Test without exit or collectible
- Test without player

#### Game

- At every move the current number of movements must be displayed in the shell 
- Collision with walls is necessary. Player can't move into walls
- Collectible must disappear when the player passes over them
- The player cant escape the map with the exit only when all collectible are collect
- W, A, S, D for move, ESC or red cross for exit

## How run

### On linux

- MiniLibX for Linux requires `xorg`, `x11` and `zlib`, therefore you will need to install: `xorg`, `libxext-dev` and `zlib1g-dev`. 
```sh
sudo apt-get update && sudo apt-get install xorg libxext-dev zlib1g-dev
```

- To compile:
```sh
./so_long map/map.ber
```

@bcano 2021
