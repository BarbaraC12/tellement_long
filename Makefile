NAME		= so_long

SRCS		= srcs/0_main.c srcs/1_check_arg.c \
				srcs/2_initialize.c srcs/3_save_map.c \
				srcs/4_check_validity.c srcs/5_draw.c \
				srcs/7_hook.c srcs/8_free_data.c \
				srcs/6_libft_1.c srcs/6_libft_2.c srcs/6_libft_3.c

OBJS_PATH	= ./objs/
TMP			= ${SRCS:.c=.o}
OBJS		= $(addprefix ${OBJS_PATH},${TMP})

INCS		= -I includes/
LINUX		= -I /usr/include -L /usr/lib/ -lmlx -Lmlx_linux -Imlx_linux -lXext -lX11 -lm -lz

CC		= cc
CFLAGS		= -Wall -Wextra -Werror -g3# -fsanitize=address


SRCS_B		=	bonus/0_main.c bonus/1_check_arg.c \
				bonus/2_initialize.c bonus/3_save_map.c \
				bonus/4_check_validity.c bonus/5_draw.c \
				bonus/7_hook.c bonus/8_free_data.c \
				bonus/6_libft_1.c bonus/6_libft_2.c \
				bonus/6_libft_3.c \


${NAME}: ${OBJS}
	echo "\033[1;93m\tCompilation MinilibX\033[0m"
	make -C mlx_linux all
		${CC} ${CFLAGS} ${OBJS} ${LINUX} -o ${NAME}
	echo "\033[1;92m\tCompilation complet\033[0m"

${OBJS_PATH}%.o: %.c
			mkdir -p ${OBJS_PATH}/srcs
			${CC} -c ${CFLAGS} -o $@ $< ${INCS}

all:	${NAME}

#OBJS_PATH_B	= ./objs/
#TMP_B		= ${SRCS_B:.c=.o}
#OBJS_B		= $(addprefix ${OBJS_PATH_B},${TMP_B})

#BONUS		= -I /usr/include -L /usr/lib/ -lmlx -Lmlx_linux -Imlx_linux -lXext -lX11 -lm -lz -DBONUS=1

#${NAME_B}: ${OBJS_B}
#	@make -C mlx_linux all
#		${CC} ${CFLAGS} ${OBJS_B} ${BONUS} -o ${NAME_B}
#	@echo "\033[1;92m\tCompilation bonus complet\033[0m"

#${OBJS_PATH_B}%.o: %.c
#			@mkdir -p ${OBJS_PATH_B}/bonus
#			${CC} -c ${CFLAGS} -o $@ $< ${INCS}

#bonus:	${NAME_B}

clean:
	rm -rf objs srcs/*.o
	echo "\033[1;93m\tObject file clean\033[0m"

fclean:	
	echo "\033[1;91m\tCleaning project\033[0m"
	make clean
	rm -rf ${NAME} #${NAME_B}
	make -C mlx_linux clean
	echo "\033[1;93m\t>>> Project clean <<<\033[0m"

re:			fclean all

.PHONY:		all clean fclean re
